# Railfence cipher algorithm
## Introduction
Also called a zigzag cipher is a form of transposition cipher. It derives its name from the way in which it is encoded. For example, if 3 "rails" and the message 'WE ARE DISCOVERED. FLEE AT ONCE' is used, the cipherer writes out:
```
W . . . E . . . C . . . R . . . L . . . T . . . E
. E . R . D . S . O . E . E . F . E . A . O . C .
. . A . . . I . . . V . . . D . . . E . . . N . .
```
The message encrypted is WECRLTEERDSOEEFEAOCAIVDEN

Another example is 'I REALLY LIKE PUZZLES', which takes the spaces when the message is being encrypted:
```
I . . . A . . . _ . . . E . . . Z . . . S
. _ . E . L . Y . L . K . _ . U . Z . E .
. . R . . . L . . . I . . . P . . . L . .
```
Now, the message encrypted is IA EZS ELYLK UZERLIPL
## Script
Encrypt and decrypt texts inputs using Railfence's algorithm
### Usage
This script must executes by console like that

`railfence.py [-h] [-e] [-d] message key alphabet spaces`

Try this and tell me what's the encrypted message:

`$ python railfence.py -d 'TBEHSETDEIIPMENYEGGRSADCSE' 7 26 false`
### Positional arguments
Also if you type -h flag, the positional arguments is being shown
```
usage: railfence.py [-h] [-e] [-d] message key alphabet spaces

Cipher and decipher texts inputs using Railfence's algorithm

positional arguments:
  message        For multiple words must put the string between ''
  key            It's the number of rails
  alphabet       Select 26, 27, 36 or 37 to enable a specific alphabet
  spaces         If you want use spaces set the argument to true else type
                 false

optional arguments:
  -h, --help     show this help message and exit
  -e, --encrypt  Use to activate the cipher
  -d, --decrypt  Use to decrypt the cipher message
```
