import argparse
import re

def en(string,spaces):
	message = string.upper()
	spaces = spaces.lower()
	if spaces == 'true':
		message_cleaned = re.sub('[^A-Z ]','',message)
	else:
		message_cleaned = re.sub('[^A-Z]','',message)

	return message_cleaned

def es(string,spaces):
	message = string.upper()
	spaces = spaces.lower()
	if spaces == 'true':
		message_cleaned = re.sub('[^A-ZÑ ]','',message)
	else:
		message_cleaned = re.sub('[^A-ZÑ]','',message)

	return message_cleaned

def en_0_9(string,spaces):
	message = string.upper()
	spaces = spaces.lower()
	if spaces == 'true':
		message_cleaned = re.sub('[^A-Z0-9 ]','',message)
	else:
		message_cleaned = re.sub('[^A-Z0-9]','',message)

	return message_cleaned

def es_0_9(string,spaces):
	message = string.upper()
	spaces = spaces.lower()
	if spaces == 'true':
		message_cleaned = re.sub('[^A-ZÑ0-9 ]','',message)
	else:
		message_cleaned = re.sub('[^A-ZÑ0-9]','',message)

	return message_cleaned

def clean(case,string,spaces):
	switch = {26:en(string,spaces),27:es(string,spaces),36:en_0_9(string,spaces),37:es_0_9(string,spaces),}

	return switch.get(case)

def output(_list):
	output = ''

	for nested_list in _list:
		for char in nested_list:
			output += char

	return output

def encrypt(string,key,alpha,spaces):
	message = clean(alpha,string,spaces)
	fence = [[] for i in range(key)] # List comprehesion to initialize the fence to encrypt
	rail = 0
	switch = 1

	# The message is being encrypted
	for char in message:
		fence[rail].append(char)
		rail += switch
		if rail == key-1 or rail == 0:
			switch *= -1

	return fence

def decrypt(string,key,alpha,spaces):
	message = clean(alpha,string,spaces)
	encrypted_message = encrypt(message,key,alpha,spaces)
	aux_fence = [[] for i in range(key)]
	rail = 0
	switch = 1
	fence = []
	size_message = len(message)
	count = 0
	_list_message = list(message)

	# The encrypted message is being appended in a auxiliar fence
	for nested_list in encrypted_message:
		for i in range(len(nested_list)):
			aux_fence[count].append(_list_message[0])
			_list_message.remove(_list_message[0])
		count += 1

	# The decrypted message is being recorded inside a list
	for i in range(size_message):
		fence += aux_fence[rail][0]
		aux_fence[rail].remove(aux_fence[rail][0])
		rail += switch

		if rail == key-1 or rail == 0:
			switch *= -1

	return fence

if __name__ == '__main__':

	parser = argparse.ArgumentParser(description='Cipher and decipher texts inputs using Railfence\'s algorithm')

	# Declare arguments to pass form console
	parser.add_argument('message',help='For multiple words must put the string between \'\'')
	parser.add_argument('key', type=int, help='It\'s the number of rails')
	parser.add_argument('alphabet', type=int, help='Select 26, 27, 36 or 37 to enable a specific alphabet')
	parser.add_argument('spaces', help='If you want use spaces set the argument to true else type false')
	parser.add_argument('-e', '--encrypt', action='store_true', help='Use to activate the cipher')
	parser.add_argument('-d', '--decrypt', action='store_true', help='Use to decrypt the cipher message')

	args = parser.parse_args()

	if args.encrypt and args.alphabet == 26 or args.alphabet == 27 or args.alphabet == 36 or args.alphabet == 37: # When the key is equivalent to one, the message encrypted are being kept same the original
		print(output(encrypt(args.message,args.key,args.alphabet,args.spaces)))
	elif args.decrypt and args.alphabet == 26 or args.alphabet == 27 or args.alphabet == 36 or args.alphabet == 37: # When the key is equivalent to one, the message encrypted are being kept same the original
		print(output(decrypt(args.message,args.key,args.alphabet,args.spaces)))
	else:
		print('Invalid alphabet!')
